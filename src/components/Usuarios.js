import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Pagination from './Pagination';

const Usuarios = () => {
  
  const [characters, setCharacters] = React.useState([]);
  const [info, setInfo] = React.useState({});

  const initialUrl = "https://rickandmortyapi.com/api/character";

  const fetchCharacters = (url) => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setCharacters(data.results);
        setInfo(data.info);
      })
      .catch((error) => console.log(error));
  };

  const onPrevious = () => {
    fetchCharacters(info.prev);
  }

  const onNext = () => {
    fetchCharacters(info.next);
  }


  React.useEffect(() => {
    fetchCharacters(initialUrl);
  }, []);

  return (
    <>
      <Pagination
        prev={info.prev} next={info.next}
        onPrevious={onPrevious}
        onNext={onNext} />
      {characters.map((item, index) => (
          <Card sx={{ maxWidth: 345 }} key={index}>
            <CardMedia
              component="img"
              height="140"
              image={item.image}
              alt="green iguana"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                {item.name}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <p>Species: {item.species}</p>
                <p>Location: {item.location.name}</p>
              </Typography>
            </CardContent>
          </Card>
        ))}
        <Pagination
          prev={info.prev} next={info.next}
          onPrevious={onPrevious}
          onNext={onNext} />
    </>
  )

}

export default Usuarios;