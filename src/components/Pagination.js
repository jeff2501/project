import { Button } from '@mui/material';
import React from 'react'

const Pagination = ({ prev, next, onPrevious, onNext }) => {

    const handlePrevious = () => {
        onPrevious();
    }

    const handleNext = () => {
        onNext();
    }

    return (
      <>
        {prev ? (
          <Button onClick={handlePrevious}>Previous</Button>
        ) : null}
        {next ? (
          <Button onClick={handleNext}>Next</Button>
        ) : null}
      </>
    )
}

export default Pagination