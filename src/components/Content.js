import React from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import CuentasBancarias from './CuentasBancarias';
import PrestamosBancarios from './PrestamosBancarios';
import Usuarios from './Usuarios';

const Content = () => {

  const [value, setValue] = React.useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (

    <Box sx={{ width: '100%', typography: 'body1', minWidth: '400px' }}>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            <Tab label="CUENTAS" value="1" />
            <Tab label="CRÉDITOS" value="2" />
            <Tab label="USUARIOS" value="3" />
          </TabList>
        </Box>

        <TabPanel value="1">
          
        </TabPanel>
        <TabPanel value="1">
          <CuentasBancarias />
        </TabPanel>
        <TabPanel value="2">
          <PrestamosBancarios />
        </TabPanel>
        <TabPanel value="3">
          <Usuarios />
        </TabPanel>
      </TabContext>
    </Box>

  )

}

export default Content;