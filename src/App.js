import React from 'react';
import logo from './logo.svg';
import './App.css';
import ProfileForm from './ProfileForm';
import ProfileFormHOC from './ProfileFormHOC';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import Content from './components/Content';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div className="wrapper">
          {/* <div className="half">
            <ProfileForm></ProfileForm>
          </div>
          <div className="half">
            <ProfileFormHOC></ProfileFormHOC>
          </div> */}

          <Content />


        </div>
      </header>
    </div>
  );
}

export default App;
