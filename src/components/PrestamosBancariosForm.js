import React from 'react';
import { Formik, Field, Form } from 'formik';
import { Box, FormControl, InputLabel, Input, Button } from '@mui/material';

const PrestamosBancariosForm = ({ editar, cuentas, setCuentas, setEditar }) => {

  console.log('editar', editar)

  return (
    <>
      <h3>Cuenta</h3>
      <Formik
        enableReinitialize={true}
        initialValues={
          editar != null ?
          cuentas[editar] : {
            nombre: "",
            saldo: ""
          }
        }
        onSubmit={async (values) => {
          if (editar == null) {
            setCuentas([
              ...cuentas,
              values
            ])
          } else {
            let cuentasEditar = cuentas;
            cuentasEditar[editar] = values;
            setCuentas(cuentasEditar);
          }
        }}
      >
        <Box
          component="form"
          sx={{
            '& > :not(style)': { m: 1 },
          }}
          noValidate
          autoComplete="off"
        >
          <Form>
            <FormControl variant="standard">
              <InputLabel htmlFor="component-simple">Motivo prestamo</InputLabel>
              <Field id="nombre" name="nombre" as={Input} />
            </FormControl>
            <FormControl variant="standard">
              <InputLabel htmlFor="component-simple">Valor a solicitar</InputLabel>
              <Field id="saldo" name="saldo" as={Input} />
            </FormControl>
            <div style={{marginTop: 10}}>
              <Button variant="contained" color="warning" type="button" onClick={() => setEditar(null)}>Cancelar</Button>
              <Button variant="contained" type="submit" style={{marginLeft: 10}}>Guardar</Button>
            </div>
          </Form>
        </Box>
      </Formik>
    </>
  )

}

export default PrestamosBancariosForm;