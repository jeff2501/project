import * as React from 'react';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/Inbox';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import PrestamosBancariosForm from './PrestamosBancariosForm';

const PrestamosBancarios = () => {

  const [open, setOpen] = React.useState(true);
  const [editar, setEditar] = React.useState(null);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [cuentas, setCuentas] = React.useState([
    {
      nombre: "Estudio",
      saldo:  750
    },
    {
      nombre: "Casa",
      saldo:  800
    },
    {
      nombre: "Celular último modelo",
      saldo:  1200
    }
  ]);

  const handleEdit = (index) => {
    setOpen(true);
    setEditar(index);
  }

  console.log('cuentas:', cuentas)

  return (

    <Box sx={{ width: '100%', maxWidth: 400, bgcolor: 'background.paper' }}>
      
      <ButtonGroup variant="contained" aria-label="outlined primary button group">
        <Button onClick={handleOpen}>Solicitar préstamo</Button>
        <Button onClick={handleClose}>Cerrar formulario</Button>
      </ButtonGroup>

      {open && 
        <PrestamosBancariosForm editar={editar} cuentas={cuentas} setCuentas={setCuentas} setEditar={setEditar} />
      }

      <nav aria-label="main mailbox folders">
        <List>
          {cuentas.map((cuenta, index) => (
            <ListItem disablePadding>
              <ListItemButton onClick={() => handleEdit(index)}>
                <ListItemIcon>
                  <InboxIcon />
                </ListItemIcon>
                <ListItemText primary={`Préstamo: ${cuenta.nombre}`} secondary={`Valor del crédito: $${cuenta.saldo}`} />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      </nav>
    </Box>

  )

}

export default PrestamosBancarios;